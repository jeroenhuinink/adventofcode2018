import * as fs from "fs";

function parseInput(
  input: string
): {
  minX: number;
  minY: number;
  maxX: number;
  maxY: number;
  scan: string[][];
} {
  let minX = Number.MAX_SAFE_INTEGER;
  let minY = Number.MAX_SAFE_INTEGER;
  let maxX = 0;
  let maxY = 0;
  const lines = input.split("\n");
  const scan: string[][] = [[]];
  scan[0][500] = "+";
  lines.forEach(line => {
    const matches = line.match(/(\w)=(\d+), (\w)=(\d+)\.\.(\d+)/);
    if (matches[1] == "x" && matches[3] == "y") {
      const x = parseInt(matches[2]);
      const y1 = parseInt(matches[4]);
      const y2 = parseInt(matches[5]);
      for (let j = y1; j <= y2; j++) {
        if (!scan[j]) {
          scan[j] = [];
        }
        scan[j][x] = "#";
      }
      if (y1 < minY) {
        minY = y1;
      }
      if (x < minX) {
        minX = x;
      }
      if (x > maxX) {
        maxX = x;
      }
      if (y2 > maxY) {
        maxY = y2;
      }
    } else {
      const y = parseInt(matches[2]);
      const x1 = parseInt(matches[4]);
      const x2 = parseInt(matches[5]);
      if (!scan[y]) {
        scan[y] = [];
      }
      for (let i = x1; i <= x2; i++) {
        scan[y][i] = "#";
      }
      if (y < minY) {
        minY = y;
      }
      if (x1 < minX) {
        minX = x1;
      }
      if (x2 > maxX) {
        maxX = x2;
      }
      if (y > maxY) {
        maxY = y;
      }
    }
  });
  return { minX, minY, maxX, maxY, scan };
}

function printScan(minX: number, maxX: number, maxY: number, scan: string[][]) {
  for (let j = 0; j <= (maxY + 1 > 50 ? 50 : maxY + 1); j++) {
    let line = "";
    for (let i = minX - 1; i <= maxX + 1; i++) {
      line += scan[j] ? scan[j][i] || "." : ".";
    }
    console.info(j.toString().padStart(3, " "), line);
  }
}

function countWater(minY: number, maxY: number, scan: string[][]): number {
  let count = 0;
  for (let j = minY; j <= maxY; j++) {
    if (scan[j]) {
      for (let i = 0; i < scan[j].length; i++) {
        const char = scan[j][i];
        if (
          char == "+" ||
          char == "~" ||
          char == "<" ||
          char == ">" ||
          char == "|"
        ) {
          count++;
        }
      }
    }
  }
  return count;
}

function solve(input: string) {
  const { minY, maxY, minX, maxX, scan } = parseInput(input);

  const noFlow = (x: number, y: number) =>
    scan[y][x] == "#" || scan[y][x] == "~";
  const canFlow = (x: number, y: number) =>
    scan[y][x] == "#" || scan[y][x] == "~";

  function flowRow(y: number) {
    if (!scan[y]) {
      scan[y] = [];
    }
    for (let i = 0; i < maxX; i++) {
      if (!scan[y][i] && scan[y-1][i] =='|') {
        scan[y][i] = '|';
      } 
    }
    const left = scan[y].indexOf('|');
    
  }
  function flow2() {
    let dropped = true;
    let bottom = false;
    let y = 1;
    let x = 500;
    while (!dropped || bottom) {
      dropped = false;
      if (!scan[y]) {
        scan[y] = [];
      }
      if (!scan[y][x]) {
        scan[y][x] = "|";
        dropped = true;
      } else if (scan[y][x] == "|") {
        if (scan[y + 1][x] == "|") {
          // going up from infinity
          y--;
        } else if (scan[y][x - 1] == "~" || scan[y][x - 1] == "#") {
          scan[y][x] = "~";
          if (scan[y][x + 1] == "#") {
            // completed layer. Go up.
            y--;
            // Find source
            while (scan[y][x] != "|") {
              x--;
            }
          }
        } else {
        }
        //bottom going up
        //going left;
        //going right;
      } else if (scan[y][x] == "~") {
      } else if (scan[y][x] == "#") {
        //going down
        //going left;
        //going right;
      }
      if (y > maxY + 1) {
        bottom = true;
        dropped = false;
        break;
      }

      if (!scan[y][x]) {
        scan[y][x] = "|";
        dropped = true;
        y++;
        break;
      }
    }
  }

  function flow(
    x: number,
    y: number,
    char: string
  ): { dropped?: boolean; bottom?: boolean } {
    if (!scan[y]) {
      scan[y] = [];
    }

    if (y > maxY + 1) {
      return { bottom: true };
    }

    if (!scan[y][x]) {
      scan[y][x] = "|";
      return { dropped: true };
    }

    if (noFlow(x, y)) {
      return {};
    }

    const down = flow(
      x,
      y + 1,
      "|"
    );

    if (down.dropped || down.bottom) {
      return down;
    }

    if (char != ">") {
      const left = flow(
        x - 1,
        y,
        "<"
      );

      if (left.dropped) {
        return left;
      }
      if (left.bottom && char == "|") {
        return left;
      }
    }

    const right = flow(
      x + 1,
      y,
      ">"
    );

    if (right.dropped || right.bottom) {
      return right;
    }

    if (char == ">" || char == "<") {
      scan[y][x] = "~";
    }
    return { dropped: false, bottom: false };
  }
  let count = 0;
  while (
    flow(
      500,
      0,
      "|"
    ).dropped
  ) {
    // printScan(minX, maxX, maxY, scan);
    if (count % 1000 == 0) {
      printScan(minX, maxX, maxY, scan);
    }
    count++;
  }

  printScan(minX, maxX, maxY, scan);

  return countWater(minY, maxY, scan);
}

let testId = 0;
function test(input: string, expected: any) {
  testId++;
  const actual = solve(input);
  if (actual != expected) {
    console.error(`${testId} failed. Expected ${expected} got ${actual}`);
    throw "fail";
  }
}

test(
  `x=495, y=2..7
y=7, x=495..501
x=501, y=3..7
x=498, y=2..4
x=506, y=1..2
x=498, y=10..13
x=504, y=10..13
y=13, x=498..504`,
  57
);

fs.readFile("input.txt", (err, data) => {
  if (err) {
    throw err;
  }

  const input = data.toString();
  console.info(solve(input));
});
