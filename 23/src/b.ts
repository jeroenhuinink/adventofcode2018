import * as fs from "fs";
import { normalize } from "path";

function parseInput(input: string) {
  return input.split("\n").map(s => {
    const matches = s.match(/<([-]?\d+),([-]?\d+),([-]?\d+)>, r=([-]?\d+)/);
    return {
      x: parseInt(matches[1]),
      y: parseInt(matches[2]),
      z: parseInt(matches[3]),
      r: parseInt(matches[4])
    };
  });
}
interface Cube {
  minX: number;
  maxX: number;
  minY: number;
  maxY: number;
  minZ: number;
  maxZ: number;
}

function solve(input: string): any {
  const nats = parseInput(input);
  const cubes = nats.map(n => ({
    minX: n.x - n.r,
    maxX: n.x + n.r,
    minY: n.y - n.r,
    maxY: n.y + n.r,
    minZ: n.z - n.r,
    maxZ: n.z + n.r
  }));

  function overlap(a: Cube, b: Cube): Cube {
    if (a.minX > b.maxX || b.minX > a.maxX) {
      return null;
    }
    if (a.minY > b.maxY || b.minY > a.maxY) {
      return null;
    }
    if (a.minZ > b.maxZ || b.minZ > a.maxZ) {
      return null;
    }
    return {
      minX: Math.min(a.minX, b.minX),
      maxX: Math.max(a.maxX, b.maxX),
      minY: Math.min(a.minY, b.minY),
      maxY: Math.max(a.maxY, b.maxY),
      minZ: Math.min(a.minZ, b.minZ),
      maxZ: Math.min(a.maxZ, b.maxZ)
    };
  }
  
  for (let start = 0; start < cubes.length; start++) {
    let overlapped = cubes[start];
    for (let i = 0; i < nats.length; i++) {
      if (overlap(overlapped, cubes[i])) {
        console.log('o');
      }
    }
  }
  return 0;
}

let testId = 0;
function test(input: string, expected: any) {
  testId++;
  const actual = solve(input);
  if (actual != expected) {
    console.error(`${testId} failed. Expected ${expected} got ${actual}`);
    throw "fail";
  }
}

test(
  `pos=<0,0,0>, r=4
pos=<1,0,0>, r=1
pos=<4,0,0>, r=3
pos=<0,2,0>, r=1
pos=<0,5,0>, r=3
pos=<0,0,3>, r=1
pos=<1,1,1>, r=1
pos=<1,1,2>, r=1
pos=<1,3,1>, r=1`,
  7
);

test(
  `pos=<10,12,12>, r=2
pos=<12,14,12>, r=2
pos=<16,12,12>, r=4
pos=<14,14,14>, r=6
pos=<50,50,50>, r=200
pos=<10,10,10>, r=5`,
  36
);

fs.readFile("input.txt", (err, data) => {
  if (err) {
    throw err;
  }

  const input = data.toString();
  console.info(solve(input));
});
