import * as fs from "fs";

function parseInput(input: string) {
  return input.split("\n").map(s => {
    const matches = s.match(/<([-]?\d+),([-]?\d+),([-]?\d+)>, r=([-]?\d+)/);
    return {
      x: parseInt(matches[1]),
      y: parseInt(matches[2]),
      z: parseInt(matches[3]),
      r: parseInt(matches[4])
    };
  });
}
function solve(input: string): any {
  const nats = parseInput(input);
  const n = nats.reduce((a, v) => (a.r > v.r ? a : v));
  return nats.reduce(
    (a, v) =>
      Math.abs(n.x - v.x) + Math.abs(n.y - v.y) + Math.abs(n.z - v.z) <= n.r
        ? a + 1
        : a,
    0
  );
  return 0;
}

let testId = 0;
function test(input: string, expected: any) {
  testId++;
  const actual = solve(input);
  if (actual != expected) {
    console.error(`${testId} failed. Expected ${expected} got ${actual}`);
    throw "fail";
  }
}

test(
  `pos=<0,0,0>, r=4
pos=<1,0,0>, r=1
pos=<4,0,0>, r=3
pos=<0,2,0>, r=1
pos=<0,5,0>, r=3
pos=<0,0,3>, r=1
pos=<1,1,1>, r=1
pos=<1,1,2>, r=1
pos=<1,3,1>, r=1`,
  7
);

fs.readFile("input.txt", (err, data) => {
  if (err) {
    throw err;
  }

  const input = data.toString();
  console.info(solve(input));
});
