import * as fs from "fs";

const infinity = Number.MAX_SAFE_INTEGER;

class Room {
  public index: number;
  public neighbors: Room[] = [];
  public x: number;
  public y: number;
  public combatant: Combatant;
  constructor(index: number, x: number, y: number, combatant?: Combatant) {
    this.index = index;
    this.combatant = combatant || null;
    this.x = x;
    this.y = y;
  }

  public toString() {
    return this.combatant ? this.combatant.type : ".";
  }

  public emptyNeighbors() {
    return this.neighbors.filter(n => n.combatant == null);
  }
}

function shortestPath(rooms: Room[], start: Room, end: Room) {
  const visited: boolean[] = [];
  const distances: number[] = [];
  for (let i = 0; i < rooms.length; i++) {
    if (i == start.index) {
      distances[i] = 0;
    } else {
      distances[i] = infinity;
    }
  }

  function visit(current: Room) {
    current
      .emptyNeighbors()
      .filter(n => !visited[n.index])
      .forEach(n => {
        if (distances[current.index] + 1 < distances[n.index]) {
          distances[n.index] = distances[current.index] + 1;
        }
      });

    visited[current.index] = true;
  }

  let unvisited = rooms.filter(r => !visited[r.index]);
  while (
    !visited[end.index] ||
    unvisited.find(f => distances[f.index] < infinity)
  ) {
    const current = unvisited.sort(
      (a, b) => distances[a.index] - distances[b.index]
    )[0];

    if (current) {
      visit(current);
    }
    unvisited = rooms.filter(r => !visited[r.index]);
  }
  let next = end;
  if (distances[end.index] != infinity) {
    while (next.neighbors.indexOf(start) == -1) {
      next = next.neighbors.sort(
        (a, b) => distances[a.index] - distances[b.index]
      )[0];
    }
  } else {
    next = null;
  }
  return { next, distance: distances[end.index] };
}

class Combatant {
  public type: "E" | "G";
  public index: number;
  public HP: number;
  public AP: number;
  public room: Room;

  constructor(index: number, type: "E" | "G", room: Room) {
    this.index = index;
    this.type = type;
    this.HP = 200;
    this.AP = 3;
    this.room = room;
  }

  public toString() {
    return `${this.index}: ${this.type}, HP: ${this.HP}`;
  }

  public move(target: Room) {
    this.room.combatant = null;
    target.combatant = this;
    this.room = target;
  }

  private findInRange(opponents: Combatant[]): Room[] {
    return opponents
      .map(o => o.room.emptyNeighbors())
      .reduce((a, v) => a.concat(v), []);
  }

  private findNext(opponents: Combatant[], rooms: Room[]): Room {
    const inRange = this.findInRange(opponents);
    const res = inRange
      .map(room => ({
        room,
        ...shortestPath(rooms, this.room, room)
      }))
      .filter(s => s.distance != infinity)
      .sort((a, b) => a.distance - b.distance)[0];

    return res ? rooms[res.next.index] : null;
  }

  public turn(combatants: Combatant[], map: Room[]) {
    const inrange = this.room.neighbors
      .map(n => n.combatant)
      .filter(c => c && c.type != this.type)
      .sort((a, b) => a.HP - b.HP || a.room.index - b.room.index)[0];
    if (inrange) {
      this.attack(inrange);
    } else {
      const opponents = combatants.filter(c => c.HP > 0 && c.type != this.type);
      const target = this.findNext(opponents, map);
      if (target) {
        if (
          Math.abs(this.room.x - target.x) + Math.abs(this.room.y - target.y) !=
          1
        ) {
          console.error(this.room.x, this.room.y, target.x, target.y);
          throw "No neighbor";
        }
        this.move(target);
        const inrange = this.room.neighbors
          .map(n => n.combatant)
          .filter(c => c && c.type != this.type)
          .sort((a, b) => a.HP - b.HP || a.room.index - b.room.index)[0];
        if (inrange) {
          this.attack(inrange);
        }
      }
    }
  }

  public attack(opponent: Combatant) {
    opponent.defend(this.AP);
  }

  public defend(AP: number) {
    this.HP -= AP;
    if (this.HP <= 0) {
      this.HP = 0;
      this.room.combatant = null;
      this.room = null;
    }
  }
}

let maxX = 0;
let maxY = 0;
function parseInput(input: string): { combatants: Combatant[]; rooms: Room[] } {
  const combatants: Combatant[] = [];
  const map = input.split("\n").map(s => s.split(""));
  maxX = map[0].length;
  maxY = map.length;
  const rooms: Room[] = [];
  const roomGrid: Room[][] = [];

  function setCombatant(room: Room, char: string) {
    if (char == "E" || char == "G") {
      const combatant = new Combatant(combatants.length, char, room);
      combatants.push(combatant);
      room.combatant = combatant;
    } else {
      if (char != ".") {
        throw "unexpected input";
      }
    }
  }

  function setNeighbors(room: Room, i: number, j: number) {
    if (roomGrid[i - 1] && roomGrid[i - 1][j]) {
      room.neighbors.push(roomGrid[i - 1][j]);
    }
    if (roomGrid[i + 1] && roomGrid[i + 1][j]) {
      room.neighbors.push(roomGrid[i + 1][j]);
    }
    if (roomGrid[i][j - 1] && roomGrid[i][j - 1]) {
      room.neighbors.push(roomGrid[i][j - 1]);
    }
    if (roomGrid[i][j + 1] && roomGrid[i][j + 1]) {
      room.neighbors.push(roomGrid[i][j + 1]);
    }
  }

  // First we will the grid to make it easier to set neighbors
  for (let i = 0; i < map.length; i++) {
    roomGrid[i] = [];
    for (let j = 0; j < map[i].length; j++) {
      if (map[i][j] != "#") {
        const room = new Room(rooms.length, j, i);
        rooms.push(room);
        roomGrid[i][j] = room;
      }
    }
  }

  let index = 0;
  for (let i = 0; i < map.length; i++) {
    for (let j = 0; j < map[i].length; j++) {
      const char = map[i][j];
      if (char != "#") {
        const room = rooms[index];
        setNeighbors(room, i, j);
        setCombatant(room, char);
        index++;
      }
    }
  }
  return { combatants, rooms };
}

function printMap(rooms: Room[]) {
  let index = 0;
  let res = "";

  for (let i = 0; i < maxY; i++) {
    for (let j = 0; j < maxX; j++) {
      if (rooms[index] && rooms[index].x == j && rooms[index].y == i) {
        const room = rooms[index];
        index++;
        res += room.toString();
      } else {
        res += "#";
      }
    }
    res += "\n";
  }
  console.info(res);
}

function printCombatants(combatants: Combatant[]) {
  combatants.forEach(c => console.info(c.toString()));
}
function checkVictory(combatants: Combatant[]) {
  const active = combatants.filter(c => c.HP > 0);
  const elves = active.filter(c => c.type == "E");
  const goblins = active.filter(c => c.type == "G");
  if (elves.length == 0 || goblins.length == 0) {
    return true;
  }
  return false;
}
function solve(input: string): any {
  const { combatants, rooms } = parseInput(input);
  let round = 0;
  let victory = false;
  while (!victory) {
    console.log(round);
    printMap(rooms);
    printCombatants(combatants);

    const order = combatants
      .filter(c => c.HP > 0)
      .sort((a, b) => a.room.y - b.room.y || a.room.x - b.room.x);
    let fullRound = true;
    for (let p = 0; p < order.length; p++) {
      const player = order[p];
      if (player.HP > 0) {
        if (victory) {
          fullRound = false;
        }
        player.turn(combatants, rooms);
        victory = checkVictory(combatants);
      }
    }

    if (fullRound) {
      round++;
    }
  }
  console.log(round, combatants.reduce((a, v) => a + v.HP, 0));
  printMap(rooms);
  printCombatants(combatants);
  return combatants.reduce((a, v) => a + v.HP, 0) * round;
}

let testId = 0;
function test(input: string, expected: any) {
  maxX = 0;
  maxY = 0;
  testId++;
  const actual = solve(input);
  if (actual != expected) {
    console.error(`${testId} failed. Expected ${expected} got ${actual}`);
    throw "fail";
  }
}

test(
  `#######
#.G...#
#...EG#
#.#.#G#
#..G#E#
#.....#
#######`,
  27730
);

test(
  `#######
#G..#E#
#E#E.E#
#G.##.#
#...#E#
#...E.#
#######`,
  36334
);

test(
  `#######
#E..EG#
#.#G.E#
#E.##E#
#G..#.#
#..E#.#
#######`,
  39514
);
test(
  `#######
#E.G#.#
#.#G..#
#G.#.G#
#G..#.#
#...E.#
#######`,
  27755
);
test(
  `#######
#.E...#
#.#..G#
#.###.#
#E#G#G#
#...#G#
#######`,
  28944
);

test(
  `#########
#G......#
#.E.#...#
#..##..G#
#...##..#
#...#...#
#.G...G.#
#.....G.#
#########`,
  18740
);
// fs.readFile("input.txt", (err, data) => {
//   if (err) {
//     throw err;
//   }

// const input = data.toString();
const input = `################################
#######################.########
#######################.########
########..#############.########
#######.....#########....#..####
#######.....##########......####
######....#..########.......#..#
#######.G...########...........#
####..GG....G######..........###
########....G..###..E.......#.E#
########...G..#....G..G.....E..#
########...G...G.G...........E.#
####....G.....#####..E......#E.#
####.####.#..#######....G.....##
####.G#####.#########..........#
####G#####..#########..........#
####.####..E#########..........#
####...#..#.#########.G........#
####.....G..#########.........##
####..G....E.#######........####
####G.........#####...##....####
#####G................###..E####
#####..####...............######
####..#####.............########
#####.#######...........########
#####.########.........#########
#####.########.....E..##########
#.....#########...#.############
#..#############....############
################....############
##################.#############
################################`;


console.info(solve(input));
// });
