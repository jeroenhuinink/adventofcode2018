function calcType(erosion: number): string {
  switch (erosion % 3) {
    case 0:
      return "."; //rocky
    case 1:
      return "="; //wet
    case 2:
      return "|"; //narrow
    default:
      throw "unexpected value for module 3";
  }
}

function calcErosion(index: number, depth: number) {
  return (index + depth) % 20183;
}

function printRegions(
  regions: { index: number; erosion: number; type: string }[][]
) {
  for (let i = 0; i < regions.length; i++) {
    console.log(regions[i].map(r => r.type).join(""));
  }
}

function solve(depth: number, x: number, y: number): any {
  const regions: { index: number; erosion: number; type: string }[][] = [];

  const createRegion = (index: number) => {
    const erosion = calcErosion(index, depth);
    const type = calcType(erosion);
    return { index, erosion, type };
  };

  for (let i = 0; i <= y; i++) {
    regions[i] = [];
    regions[i][0] = createRegion(i * 48271);
  }

  for (let i = 0; i <= x; i++) {
    regions[0][i] = createRegion(i * 16807);
  }

  regions[0][0] = createRegion(0);
  regions[0][0].type = "M";

  for (let i = 1; i <= y; i++) {
    for (let j = 1; j <= x; j++) {
      regions[i][j] = createRegion(
        regions[i - 1][j].erosion * regions[i][j - 1].erosion
      );
    }
  }
  regions[y][x] = createRegion(0);
  regions[y][x].type = "T";
  
  printRegions(regions);
  return regions.reduce(
    (sum, r) =>
      sum +
      r.reduce((a, c) => a + (c.type == "=" ? 1 : c.type == "|" ? 2 : 0), 0),
    0
  );;
}

let testId = 0;
function test(depth: number, x: number, y: number, expected: number) {
  testId++;
  const actual = solve(depth, x, y);
  if (actual != expected) {
    console.error(`${testId} failed. Expected ${expected} got ${actual}`);
    throw "fail";
  }
}

test(510, 10, 10, 114);

console.info(solve(10647, 7, 770));
