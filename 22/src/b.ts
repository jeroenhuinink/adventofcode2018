import { stringify } from "querystring";

function calcType(erosion: number): string {
  switch (erosion % 3) {
    case 0:
      return "."; //rocky
    case 1:
      return "="; //wet
    case 2:
      return "|"; //narrow
    default:
      throw "unexpected value for module 3";
  }
}

function travelTime(from: State, to: State) {
  if (from.x == to.x && from.y == to.y) {
    if (from.gear != to.gear) {
      return 7;
    } else {
      throw "unexpected travels";
    }
  }
  if (
    from.gear == to.gear &&
    ((Math.abs(from.x - to.x) == 1 && from.y == to.y) ||
      (Math.abs(from.y - to.y) == 1 && from.x == to.x))
  ) {
    return 1;
  } else {
    throw "unexpected travels";
  }
}

function calcErosion(index: number, depth: number) {
  return (index + depth) % 20183;
}

function printRegions(
  regions: { index: number; erosion: number; type: string }[][]
) {
  for (let i = 0; i < regions.length; i++) {
    console.log(regions[i].map(r => r.type).join(""));
  }
}
interface State {
  index: number;
  gear: string;
  type: string;
  x: number;
  y: number;
  next: State[];
}

const infinity = Number.MAX_SAFE_INTEGER;

function shortestPath(states: State[], start: State, end: State) {
  const visited: boolean[] = [];
  const distances: number[] = [];
  for (let i = 0; i < states.length; i++) {
    if (i == start.index) {
      distances[i] = 0;
    } else {
      distances[i] = infinity;
    }
  }

  function visit(current: State) {
    current.next
      .filter(n => !visited[n.index])
      .forEach(n => {
        if (distances[current.index] + 1 < distances[n.index]) {
          distances[n.index] =
            distances[current.index] + travelTime(current, n);
        }
      });

    visited[current.index] = true;
  }

  let unvisited = states.filter(r => !visited[r.index]);
  while (
    !visited[end.index] ||
    unvisited.find(f => distances[f.index] < infinity)
  ) {
    const current = unvisited.sort(
      (a, b) => distances[a.index] - distances[b.index]
    )[0];

    if (current) {
      visit(current);
    }
    unvisited = states.filter(r => !visited[r.index]);
  }
  let next = end;
  if (distances[end.index] != infinity) {
    while (next.next.indexOf(start) == -1) {
      next = next.next.sort(
        (a, b) => distances[a.index] - distances[b.index]
      )[0];
    }
  } else {
    next = null;
  }
  return { next, distance: distances[end.index] };
}

function solve(depth: number, x: number, y: number): any {
  const regions: {
    index: number;
    erosion: number;
    type: string;
    states: State[];
  }[][] = [];

  const createRegion = (index: number) => {
    const erosion = calcErosion(index, depth);
    const type = calcType(erosion);
    return { index, erosion, type, states: [] as State[] };
  };

  for (let i = 0; i <= 1000; i++) {
    regions[i] = [];
    regions[i][0] = createRegion(i * 48271);
  }

  for (let i = 0; i <= 1000; i++) {
    regions[0][i] = createRegion(i * 16807);
  }

  regions[0][0] = createRegion(0);
  regions[0][0].type = "M";

  for (let i = 1; i <= y; i++) {
    for (let j = 1; j <= x; j++) {
      regions[i][j] = createRegion(
        regions[i - 1][j].erosion * regions[i][j - 1].erosion
      );
    }
  }
  regions[y][x] = createRegion(0);
  regions[y][x].type = "T";
  const states = createStates(regions);

  printRegions(regions);
  return regions.reduce(
    (sum, r) =>
      sum +
      r.reduce((a, c) => a + (c.type == "=" ? 1 : c.type == "|" ? 2 : 0), 0),
    0
  );
}

let testid = 0;
function createStates(
  regions: { index: number; erosion: number; type: string; states: State[] }[][]
) {
  const states: State[] = [];
  let mt: State = null;
  let ft: State = null;
  for (let i = 0; i < regions.length; i++) {
    for (let j = 0; j < regions[i].length; j++) {
      const s = { type: regions[i][j].type, x: j, y: i, next: [] as State[] };
      switch (regions[i][j].type) {
        case "M":
          mt = { ...s, index: states.length, type: ".", gear: "t" };
          states.push(mt);
          regions[i][j].states.push(mt);
          const mc = { ...mt, index: states.length, gear: "c" };
          states.push(mc);
          regions[i][j].states.push(mc);
          mt.next.push(mc);
          mc.next.push(mt);
          break;
        case "T":
          ft = { ...s, index: states.length, type: ".", gear: "t" };
          states.push(ft);
          const tc: State = {
            ...ft,
            index: states.length,
            gear: "c"
          };
          states.push(tc);
          ft.next.push(tc);
          tc.next.push(ft);
          break;
        case ".":
          const rc = { ...s, index: states.length, gear: "c" };
          states.push(rc);
          const rt = { ...s, index: states.length, gear: "t" };
          states.push(rt);
          rc.next.push(rt);
          rt.next.push(rc);
          break;
        case "=":
          const wc = { ...s, index: states.length, gear: "c" };
          states.push(wc);
          const wn = { ...s, index: states.length, gear: "n" };
          states.push(wn);
          wn.next.push(wc);
          wc.next.push(wn);
          break;
        case "|":
          const nt = { ...s, index: states.length, gear: "t" };
          states.push(nt);
          const nn = { ...s, index: states.length, gear: "n" };
          states.push(nn);
          nn.next.push(nt);
          nt.next.push(nn);
          break;
      }

    }
  }
  for (let i = 0; i < regions.length; i++) {
    for (let j = 0; j < regions[i].length; j++) {

    }
  }
  return states;
}

function test(depth: number, x: number, y: number, expected: number) {
  testId++;
  const actual = solve(depth, x, y);
  if (actual != expected) {
    console.error(`${testId} failed. Expected ${expected} got ${actual}`);
    throw "fail";
  }
}

test(510, 10, 10, 45);

console.info(solve(10647, 7, 770));
